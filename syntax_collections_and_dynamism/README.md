# Notions Collections and Dynamism

## Enums

```dart
// Enum to types of payment
enum PaymentType { CREDIT, DEBIT, MONEY }
```

## Switch case

```dart
switch (payment) {
  case PaymentType.CREDIT:
    print('Credit');
    break;
  case PaymentType.DEBIT:
    print('Debit');
    break;
  case PaymentType.MONEY:
    print('Money');
    break;
  default:
    print('Invalid');
}
```

In Dart you'll need to put a `break` after each case, otherwise it will return an error.

## Collections

### Sets

```dart
// Sets are unordered collections of unique items
Set<String> countries = <String>{};
countries.add('Brazil');
countries.add('USA');
countries.add('China');
countries.remove('China');
print(countries);
```

* `Functions`
* add - Add an item to the set
* remove - Remove an item from the set
* clear - Remove all items from the set
* contains - Check if an item is in the set
* addAll - Add multiple items to the set

## Ireterable

```dart
// Iterable is a collection of items that can be accessed sequentially
Iterable<String> countries = ['Brazil', 'USA', 'China'];
print(countries);
```

* `Functions`
* first - Returns the first item in the collection
* last - Returns the last item in the collection
* isEmpty - Returns true if the collection is empty
* isNotEmpty - Returns true if the collection is not empty
* length - Returns the number of items in the collection
* indexOf - Returns the index of the first occurrence of an item in the collection

## Maps

```dart
// Maps are collections of key-value pairs
Map<String, String> capitals = {
  'Brazil': 'Brasilia',
  'USA': 'Washington',
  'China': 'Beijing'
};
print(capitals);

Map<String, dynamic> person = {
  'name': 'John',
  'age': 30,
  'height': 1.80,
  'isMarried': false
};
print(person);
```

### Iterator

```dart
// Iterator is an object that can be used to iterate through the elements of a collection
Iterator<String> countries = ['Brazil', 'USA', 'China'].iterator;
while (countries.moveNext()) {
  print(countries.current);
}
```

## dynamic vrsus var

```dart
// dynamic
dynamic name = 'John';
name = 30;
name = false;
print(name);

// var
var name = 'John';
name = 30; // Error
// you can't change the type of a variable declared with var
```

## static modifier

works exactly like the static keyword in Java

## encapsulation

```dart
// encapsulation
class Person {
  String name;
  int age;
  double height;
  bool isMarried;

  Person(this.name, this.age, this.height, this.isMarried);

  void showName() {
    print(this.name);
  }
}
```

### Getters and Setters

```dart
// Getters and Setters
class Person {
  String _name;
  int _age;
  double _height;
  bool _isMarried;

  Person(this._name, this._age, this._height, this._isMarried);

  String get name {
    return this._name;
  }

  set name(String name) {
    this._name = name;
  }

  void showName() {
    print(this._name);
  }

  // Getters and Setters with arrow functions
  get age => this._age;
  set age(age) => this._age = age;
}

void main(List<String> args) {
  Person person = Person('John', 30, 1.80, false);
  person.showName();
  person.name = 'Mary';
  print(person.name);
  print(person.age);
  person.age = 31; // Setters -> In dart you can use the setters like a variable
  print(person.age);
}
```
