import 'package:syntax_collections_and_dynamism/syntax_collections_and_dynamism.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });
}
