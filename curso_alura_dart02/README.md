# Notions OOP

## Classes

Create and instance of a class

```dart
class Person {
  String name;
  int age;
  double height;

  // Constructor
  Person(this.name, this.age, this.height);

  // Named constructor
  Person.born(this.name, this.height) {
    age = 0;
    print('$name born');
    sleep();
  }

  // Named constructor
  Person.growUp(this.name, this.age, this.height) {
    print('$name born');
    sleep();
    for (var i = 0; i < age; i++) {
      grow();
    }
  }

  void sleep() {
    print('$name is sleeping');
  }

  void grow() {
    height += 0.5;
  }
}

void main() {
  // Instance of the class
  Person p1 = Person('John', 30, 1.80);
}
```

### Types of constructors

* Positioanl optional constructor - `Person(this.name, [this.age, this.height]);`
* Named optional constructor - `Person(this.name, {this.age, this.height});`
* Required positional constructor - `Person(this.name, this.age, this.height);`
* Required named constructor - `Person({required this.name, required this.age, required this.height});`

## Heritage

```dart
class Animal {
  int age;
  String name;
  bool isAlive; // The constraint is to start the boolean variable with 'is'

  Animal(this.age, this.name, this.isAlive);

  void sleep() {
    print('$name is sleeping');
  }

  void eat() {
    print('$name is eating');
  }
}

class Cat extends Animal {
  int lives;

  Cat(this.lives, int age, String name, bool isAlive)
      : super(age, name, isAlive);

  void meow() {
    print('$name is meowing');
  }

  @override
  String toString() {
    return 'Cat: $name, Age: $age, Lives: $lives';
  }
}

void main(List<String> arguments) {
  Animal frajola = Cat(7, 7, 'Frajola', true);

  print(frajola);
}
```

## Abstract classes

```dart
abstract class Cake {
  void separateIngredients();
  void bake();
  void cook();
}

class ChocolateCake implements Cake {
  @override
  void separateIngredients() {
    print('Separate chocolate ingredients');
  }

  @override
  void bake() {
    print('Bake');
  }

  @override
  void cook() {
    print('Cook');
  }
}
```

## Funtions/methods

```dart
bool funcIsAldult(int age) { // It's a constraint to name the function with the start func
  return age >= 18;
}

// optional parameters
void funcShowPerson(String name, {int? age}) {
  if(age != null) {
    print('Name: $name, Age: $age');
  } else {
    print('Name: $name');
  }
}
// call the function
// optional parameters don't need to be passed in the same order of the function signature
funcShowPerson('John', age: 30); // You'll need to pass the name of the optional parameter and the value

// Default parameters
void funcShowPerson(String name, {int age = 0}) {
  print('Name: $name, Age: $age');
}
// call the function
funcShowPerson('John'); // You don't need to pass the parameter with default value

// Required named parameters
void funcShowPerson({required String name, required int age}) {
  print('Name: $name, Age: $age');
}

// call the function
funcShowPerson(name: 'John', age: 30); // You'll need to pass the name of the required named parameter and the value
```
