abstract class Cake {
  void separateIngredients();
  void bake();
  void cook();
}

class ChocolateCake implements Cake {
  @override
  void separateIngredients() {
    print('Separate ingredients');
  }

  @override
  void bake() {
    print('Bake for 30 minutes');
  }

  @override
  void cook() {
    print('Cook for 15 minutes');
  }
}

void main(List<String> arguments) {
  ChocolateCake cake = ChocolateCake();

  cake.separateIngredients();
  cake.bake();
  cake.cook();
}
