# Notions of alura course (And doc)

## Types

### Built-in types

* **Numbers**(int, double)
* **Strings** (String)
* **Booleans** (bool)
* **Lists** (List, also known as arrays)
* **Sets** (Set)
* **Maps**(Map)
* **Runes** (Runes; often replaced by the characters API) strings with emoticons
* **Symbols** (Symbol)
* The value **null** (Null)

### Some anothers types of Dart

* **Object**: The superclass of all Dart classes exept **Null**.
* **Enum**: The superclass of all enums.
* **Future** and Stream: Used in asynchrony support.
* **Iterable**: Used in for-in loops and in synchronous generator functions.
* **Never**: Indicates that an expression can never successfully finish evaluating. Most often used for functions that alaways throw a exception.
* **dynamic**: Indicates that you want to disble static checking. Usually you should use Object or Object? instead.
* **void**: indicates that a value is never used. Often used as return type.

## How to create a dynamic list?

```dart
void mian(List<String> args) {
  List<dynamic> people = [] ;
}
```

## var, const, final

```dart
void mian(List<String> args){
  const double pi = 3.14;
  final String earthFormat; // I can define the value before the declarations. Just one time

  earthFromat = 'globe';

  var variable; // dynamic variable. Constants and Finals can't be dynamic
}
```

## input/output

```dart
import 'dart:io';

void main(List<String> args) {
  print('Please, type your name: ');
  var name = stdin.readLineSync();

  print('Hello, $name!');
}

```

## String interpolation

```dart
void main(List<String> args) {
  int age = 31;
  String name = 'John Doe';
  List<String> guests = ['Guilherme', 'Daniel', 'João'];

  print(
      'Hello, $name! You are $age years old.'); // just pass a bariable before a dollar sign
  print(
      'Your guest are: ${guests[0]}'); // lists you need to pass  curly brakets to interpolate
}
```

## Arithmetic operators

```dart
void main(List<String> args) {
  int num1 = 1;
  int num2 = 2;
  int result;

  // adition
  result = num1 + num2;
  // subtration
  result = num1 - num2;
  // multiplication
  result = num1 * num2;
  // division
  result = num1 / num2;
  // division returning integer
  result = num1~/num2;
  // mod
  result = num1 % um2;
  // incremt
  num1++;
  // decrement
  num2--;
}
```

## if/else

```dart
import 'dart:io';

void main(List<String> args) {
  print('Enter your age');
  var input = stdin.readLineSync();
  var idade = int.parse(input!);

  if (idade >= 50) {
    print('Old');
  } else if (idade >= 18) {
    print('Adult');
  } else {
    print('Young');
  }
}
```

## switch ase

```dart
switch (expression) {
  case ONE:
    {
      statement(s);
    }
  break;
  
  case TWO:
    {
      statement(s);
    }
  break;
  
  default:
    {
      statement(s);
    }
}
```

## for/ for in loops

```dart
// traditional
void main(List<String> args) {
  for (int i = 0; i < 10; i++) {
    print(i);
  }
}


// for all elements of a list
void main(List<String> args) {
  List<String> names = ['Daniel', 'Thiago', 'Rafael'];

  for (String name in names) {
    print(name);
  }
}

```

## while/do while loops

```dart
void main(List<String> args) {
  int count = 0;

  while (count < 10) {
    print("$count");
    count++;
  }

  do {
    print('$count');
    count--;
  } while (count > 0);
}
```

## break and continue statement

```dart
void main(List<String> args) {
  int count = 1;

  while (count < 10) {
    print(count);

    if (count == 4) {
      break;
    }

    count++;
  }

  for (int i = 0; i < 10; i++) {
    if (i == 0 || i % 2 != 0) {
      continue;
    }
    print(i);
  }
}
```

## List functions

### **sublist()**

Returns a new list containing a range of elements from the original list

```dart
void main(List<String> args) {
  var numbers = [1, 2, 3, 4, 5]; 
  var subList = numbers.sublist(1, 3); // => [2, 3]
}
```

### **forEach()**

Runs a function on each element in the list

```dart
void main(List<String> args) {
  List<String> fruits = [‘banana’, ‘pineapple’, ‘watermelon’];
  fruits.forEach((fruit) => print(fruit))
}
```

### **map()**

Produces a new list after transforming each element in a given list

```dart
void main(List<String> args) {
  var fruits = [‘banana’, ‘pineapple’, ‘watermelon’];
  var mappedFruits = fruits.map((fruit) => 'I love $fruit')
  mapperFruits.forEach((fruit) => print(fruit))
}
```

### **contains()**

Checks to comfirm that the given elements is in the list

```dart
void main(List<String> args) {
  var numbers = [1, 2, 3, 4];
  print(numbers.contains(2)); // => true
}
```

### **sort()**

Order the elements baed on the provided ordering function

```dart
numbers.sort((num1, num2) => num1 - num2); // => [1, 2, 3, 4, 5]
```

### **reduce()**, **fold()**

Compress the elements to a single value, using the given function

```dart
void main(List<String> args) {
  var numbers = [1, 2, 3, 4, 5];
  var sum = numbers.reduce((cur, next) => curr + next);
  print(sum);

  const initivalValue = 10;
  var secondSum = numbers.fold(initialValue, (curr, next) => curr + next);
  print(secondSum)
}
```

### **every()**

Confirms that every element satisfies the test

```dart
void main(List<String> args){
  List<Map<String, dynamic>> users = [
    { “name”: ‘John’, “age”: 18 },
    { “name”: ‘Jane’, “age”: 21 },
    { “name”: ‘Mary’, “age”: 23 },
  ];

  var is18AndOver = users.every((user) => user["age"] >= 18);
  print(is18AndOver);

  var hasNamesWithJ = users.every((user) => user["name"].startsWith('J'));
  print(hasNamesWithJ);
}
```

### **where()**, **firstWhere()**, **singleWhere()**

Returns a collection of elements (iterable) that satisfy a test.

```dart
void main(List<String> args) {
  List<Map<String, dynamic>> users = [
    {"name": "John", "age": 18},
    {"name": "John", "age": 21},
    {"name": "John", "age": 23},
  ];

  var over21s = users.where((user) => user["age"] > 21);
  print(over21s.length); // => 1

  var nameJ = users.firstWhere((user) => user["name"].startsWith('J'));
  print(nameJ); // => {name: John, age: 18}

  var under18s =
    users.singleWhere((user) => user["age"] < 18, orElse: () => {});
  print(under18s);
}
```

### **take()**, **skip()**

Returns a collection while including or skipping elements

```dart
void main(List<String> args) {
  var fiboNumbers = [1, 2, 3, 5, 8, 13, 21];
  print(fiboNumbers.take(3));
  print(fiboNumbers.take(5));
  print(fiboNumbers.take(3).skip(2).take(1));
}
```

### **List.from()**

Create a new list from the given collection

```dart
void main(List<String> args) {
  var fiboNumbers = [1, 2, 3, 5, 8, 13, 21];
  var cloneFiboNumbers = List.from(fiboNumbers);
  print(cloneFiboNumbers);
}
```

### **expand()**

Expands each element into zero or more elements

```dart
void main(List<String> args) {
  var pairs = [
    [1, 2],
    [3, 4]
  ];
  var flattened = pairs.expand((pair) => pair).toList();
  print(flattened);

  var input = [1, 2, 3];
  var duplicated = input.expand((i) => [i, i]).toList();
  print(duplicated);
}
```
